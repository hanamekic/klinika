﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Zadaca2.Model;

namespace Zadaca2
{
    public partial class DoktorForma : Form
    {
         Klinika klinika;

        public DoktorForma(Klinika k)
        {
            InitializeComponent();
            klinika = k;
        }

        private Karton pretraga()
        {
            Karton k = klinika.pretragaPoKartonima(textBox1.Text);
           
            return k;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Karton p = pretraga();
            if (p != null)
            {
                textBox2.Text = (p.dajPacijentaIzKartona().dajIme());
                textBox3.Text = (p.dajPacijentaIzKartona().dajPrezime());
                textBox4.Text = (p.dajPacijentaIzKartona().dajAdresu());

            if (p.dajPacijentaIzKartona().Pp != null)
            {
                button2.Visible = true;
            }
            if (p.dajPacijentaIzKartona().dajJelPreminuo())
            {
                button3.Visible = true;
            }
        }
            else
            {
                toolStripStatusLabel1.Text = ("Nepostojeci maticni broj!");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Terapija ter = new Terapija(klinika);
            Karton p = pretraga();
            ter.Show();
      //      ter.prikaz(p.dajTerapije());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            PrvaPomoc pp = new PrvaPomoc(klinika);
            Karton k = pretraga();
            pp.Show();
            pp.prikazi(k.dajPacijentaIzKartona());
        }

        private void button3_Click(object sender, EventArgs e)
        {
            InfoOSmrti sm = new InfoOSmrti(klinika);
            Karton k=pretraga();
            sm.Show();
            sm.prikaz(k.dajPacijentaIzKartona());
        }

        private void button5_Click(object sender,EventArgs e)
        {
            Pacijent p= klinika.pretragaPoJMBG(textBox12.Text);

            if (p == null)
            {
                this.errorProvider1.SetError(label20, "Neispravan maticni broj");
            }

            int i = checkedListBox1.SelectedIndex + 1;
            p.dajZeljeneORdinacije().Add(i);
            klinika.incrementById(i);
            this.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Karton k = klinika.pretragaPoKartonima(textBox13.Text);
            int c=0;
            if (radioButton3.Checked) c = 1;
            k.dodajTerapiju(textBox14.Text,c, textBox15.Text);
        }

    }
}
