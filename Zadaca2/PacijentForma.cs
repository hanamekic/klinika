﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Zadaca2.Model;

namespace Zadaca2
{
    public partial class PacijentForma : Form
    {
        private Klinika klinika;

        public Klinika Klinika { get => klinika; set => klinika = value; }

        public PacijentForma(Klinika k)
        {
            InitializeComponent();
            Klinika = k;
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            Ordinacija o = klinika.getById(Convert.ToInt32(numericUpDown1.Value));
            richTextBox1.AppendText(o.getId()+". " + o.getIme()+ " Trenutno ceka: "+o.getBrojLjudi()+" ljudi.");

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Pacijent p = klinika.pretragaPoJMBG(textBox1.Text);
            if (p == null)
            {
                this.errorProvider1.SetError(label3, "Nepostojeci pacijent");
            }
            else
            {

            }

        }
    }
}
