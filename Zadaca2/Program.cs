﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zadaca2
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Klinika klinika = new Klinika();
            klinika.dodajPacijenta("hana", "mekic", "02.04.1998", "0204998175024", 'z', "trg", "nema", "02032019");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1(klinika));
        }
    }
}
