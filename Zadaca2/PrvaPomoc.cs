﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Zadaca2.Model;

namespace Zadaca2
{
    public partial class PrvaPomoc : Form
    {
         Klinika klinika;
        public PrvaPomoc(Klinika k)
        {
            InitializeComponent();
            klinika = k;
        }

        public void prikazi(Pacijent p)
        {
            textBox1.Text = (p.Pp.dajOpis());
            textBox2.Text = (p.Pp.dajRazlog());
        }

        private void button1_Click(object sender, EventArgs e)
        {

            this.Close();
        }
    }
}
