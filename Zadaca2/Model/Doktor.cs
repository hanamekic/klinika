﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadaca2.Model
{
   public class Doktor
    {
        string ime;
        string prezime;
        int plata;
        int soba;
        string user;
        string sifra;

        public Doktor(string ime, string prezime, int plata, int soba, string user, string sifra)
        {
            this.Ime = ime;
            this.Prezime = prezime;
            this.Plata = plata;
            this.Soba = soba;
            this.user = user;
            this.sifra = sifra;
        }

        public string Ime { get => ime; set => ime = value; }
        public string Prezime { get => prezime; set => prezime = value; }
        public int Plata { get => plata; set => plata = value; }
        public int Soba { get => soba; set => soba = value; }
        public string User { get => user; set => user = value; }
        public string Sifra { get => sifra; set => sifra = value; }

        public void povecajPlatu()
        {
            plata = plata + 20;
        }
    }
}
