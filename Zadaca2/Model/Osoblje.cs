﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadaca2.Model
{
   public class Osoblje
    {
        string ime;
        string prezime;
        string jmbg;
        string user;
        string sifra;

        public Osoblje(string ime, string prezime, string jmbg, string user, string sifra)
        {
            this.Ime = ime;
            this.Prezime = prezime;
            this.jmbg = jmbg;
            this.user = user;
            this.sifra = sifra;
        }

        public string Ime { get => ime; set => ime = value; }
        public string Prezime { get => prezime; set => prezime = value; }
        public string Jmbg { get => jmbg; set => jmbg = value; }
        public string User { get => user; set => user = value; }
        public string Sifra { get => sifra; set => sifra = value; }
    }
}
