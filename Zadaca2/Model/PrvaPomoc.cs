﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadaca2.Model
{
    class PrvaPomoc
    {
        string opis;
        string razlog;
        string vrijemeSmrti;
        string uzrokSmrti;
        string vrijemeObdukcije;

        public PrvaPomoc(string opis, string razlog)
        {
            this.opis = opis;
            this.razlog = razlog;
        }
        public PrvaPomoc(string opis, string razlog, string vrijemeSmrti, string uzrokSmrti, string vrijemeObdukcije)
        {
            this.opis = opis;
            this.razlog = razlog;
            this.vrijemeSmrti = vrijemeSmrti;
            this.uzrokSmrti = uzrokSmrti;
            this.vrijemeObdukcije = vrijemeObdukcije;
        }

        public string dajOpis()
        {
            return opis;
        }

        public string dajRazlog()
        {
            return razlog;
        }

        public string dajVrijemeSmrti()
        {
            return vrijemeSmrti;
        }

        public string dajUzrok()
        {
            return uzrokSmrti;
        }

        public string dajVrijemeObdukcije()
        {
            return vrijemeObdukcije;
        }
    }
}
