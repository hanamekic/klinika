﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadaca2.Model
{
    public class Karton
    {
        Pacijent p;
        List<Terapija> t = new List<Terapija>();

        public Karton(Pacijent p, List<Terapija> t)
        {
            this.p = p;
            this.t = t;
        }

        public List<Terapija> dajTerapije()
        {
            return t;
        }

        public Pacijent dajPacijentaIzKartona()
        {
            return p;
        }

        public void dodajTerapiju(string opis, int dugorocna, string datum)
        {
            Terapija terapija = new Terapija(opis, dugorocna, datum);
            t.Add(terapija);
        }

        public int dajBrTerapije()
        {
            if (t.Count > 0) return t.Capacity;
            return 0;
        }

        public void printanje()
        {
            Console.WriteLine("Trazeni pacijent je: " + p.dajIme() + " " + p.dajPrezime());
            Console.WriteLine("Datum rodjenja: " + p.dajDatumRodjenja());
            Console.WriteLine("Spol: " + p.dajSpol());
            Console.WriteLine("Adresa: " + p.dajAdresu());
            Console.WriteLine("Bracno stanje: " + p.dajBrak());
            Console.WriteLine("Datum prijema pacijenta: " + p.dajDatumPrijema());
            Console.WriteLine("\nAnamneza: ");
            Console.WriteLine("Trenutna bolest: " + p.dajTrenutnu());
            Console.WriteLine("Alegrije: " + p.dajAlerg());
            Console.WriteLine("Ranije bolesti: " + p.dajRanije());
            Console.WriteLine("Zdravstveno stanje porodice: " + p.dajZdravPorodice());
            Console.WriteLine("Zakljucak: " + p.dajZakljucak());

            for (int i = 0; i < dajBrTerapije(); i++)
            {
                Console.WriteLine("\nTerapija {0}" + (i + 1) + ": ");
                Console.WriteLine("Opis terapije: " + t[i].dajOpisTerapije());
                Console.WriteLine("Da li je terapija dugorocna: " + Convert.ToBoolean(t[i].dajJelDugorocna()));
                Console.WriteLine("Datum propisivanja terapije: " + t[i].dajDatumTerapije());
                if (p.dajJelHitan())
                {
                    Console.WriteLine("Ukazana je prva pomoc.");
                    Console.WriteLine("Opis prve pomoci: " + p.Pp.dajOpis());
                    Console.WriteLine("Razlog davanja prve pomoci: " + p.Pp.dajRazlog());
                    if (p.dajJelPreminuo())
                    {
                        Console.WriteLine("Vrijeme smrti: " + p.Pp.dajVrijemeSmrti());
                        Console.WriteLine("Uzrok smrti: " + p.Pp.dajUzrok());
                        Console.WriteLine("Vrijeme obdukcije: " + p.Pp.dajVrijemeObdukcije());
                        Console.WriteLine("\n");
                    }
                }
            }
        }
    }
}
