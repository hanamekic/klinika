﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadaca2.Model
{
   public class Terapija
    {
        string opis;
        int dugorocna;
        string datum;
        int brTerapija = 0;

        public Terapija(string opis, int dugorocna, string datum)
        {
            this.opis = opis;
            this.dugorocna = dugorocna;
            this.datum = datum;
            brTerapija++;
        }

        public string Opis { get => opis; set => opis = value; }
        public int Dugorocna { get => dugorocna; set => dugorocna = value; }
        public string Datum { get => datum; set => datum = value; }

        public string dajOpisTerapije()
        {
            return opis;
        }

        public int dajBrTerapija()
        {
            return brTerapija;
        }

        public int dajJelDugorocna()
        {
            return dugorocna;
        }

        public string dajDatumTerapije()
        {
            return datum;
        }
    }
}
