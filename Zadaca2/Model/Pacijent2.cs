﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadaca2.Model
{
    public partial class Pacijent
    {
        public void dodajAnamnezu(string trenutnaBolest, string alergije, string ranijeBolesti, string zdravstvenoStanjePorodice, string zakljucak)
        {
            a = new Anamneza(trenutnaBolest, alergije, ranijeBolesti, zdravstvenoStanjePorodice, zakljucak);
        }

        public string dajRanije()
        {
            return a.dajRanijeBolesti();
        }

        public string dajZakljucak()
        {
            return a.dajZakljucak();
        }

        public string dajZdravPorodice()
        {
            return a.dajZdravljePorodice();
        }

        public string dajTrenutnu()
        {
            return a.dajTrenutnuBolest();
        }

        public string dajAlerg()
        {
            return a.dajAlergije();
        }
        public int dajPlacanje()
        {
            return plati;
        }

        public void povecajPlacanje(int x)
        {
            plati = plati + x;
        }

        public void staviPlacanjeNaNulu()
        {
            plati = 0;
        }
    }
}
