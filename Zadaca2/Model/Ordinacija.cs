﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadaca2.Model
{
   public class Ordinacija
    {
        string naziv;
        int brojLjudi;
        bool aparat;
        bool doktor;
        int id;

        public Ordinacija(string naziv, int brojLjudi, bool aparat, bool doktor, int id)
        {
            this.naziv = naziv;
            this.brojLjudi = brojLjudi;
            this.aparat = aparat;
            this.doktor = doktor;
            this.id = id;
        }

        public string getIme()
        {
            return naziv;
        }

        public int getId()
        {
            return id;
        }

        public bool radiLiAparat()
        {
            return aparat;
        }

        public bool imaLiDoktora()
        {
            return doktor;
        }

        public void inkrement()
        {
            brojLjudi++;
        }

        public void dekrement()
        {
            brojLjudi--;
        }

        public int getBrojLjudi()
        {
            return this.brojLjudi;
        }
    }
}
