﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadaca2.Model
{
   public class Anamneza
    {
        string trenutnaBolest;
        string alergije;
        string ranijeBolesti;
        string zdravstvenoStanjePorodice;
        string zakljucak;

        public Anamneza(string trenutnaBolest, string alergije, string ranijeBolesti, string zdravstvenoStanjePorodice, string zakljucak)
        {
            this.trenutnaBolest = trenutnaBolest;
            this.alergije = alergije;
            this.ranijeBolesti = ranijeBolesti;
            this.zdravstvenoStanjePorodice = zdravstvenoStanjePorodice;
            this.zakljucak = zakljucak;
        }

        public string dajTrenutnuBolest()
        {
            return trenutnaBolest;
        }

        public string dajAlergije()
        {
            return alergije;
        }

        public string dajRanijeBolesti()
        {
            return ranijeBolesti;
        }

        public string dajZdravljePorodice()
        {
            return zdravstvenoStanjePorodice;
        }

        public string dajZakljucak()
        {
            return zakljucak;
        }

    }
}
