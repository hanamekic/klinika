﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Zadaca2.Model
{
    public partial class Pacijent
    {
        Image slika;
        private string imePac;
        private string prezimePac;
        private string datumrodjPac;
        private string jmbgPac;
        private char spolPac;
        private string adresaPac;
        private string brakPac;
        private string datum_prijemaPac;
        private bool hitan;
        private PrvaPomoc pp;
        bool preminuo;
        Anamneza a;
        List<int> zeljeneOrdinacije = new List<int>();
        private int plati;

        internal PrvaPomoc Pp { get => pp; set => pp = value; }

        public Pacijent(string ime, string prezime, string datumrodj, string jmbg, char spol, string adresa, string brak, string datum_prijema)
        {
            imePac = ime;
            prezimePac = prezime;
            datumrodjPac = datumrodj;
            jmbgPac = jmbg;
            spolPac = spol;
            adresaPac = adresa;
            brakPac = brak;
            datum_prijemaPac = datum_prijema;
        }

        public Pacijent()
        {
        }

        public List<int> dajZeljeneORdinacije()
        {
            return zeljeneOrdinacije;
        }
        
        public string dajDatumPrijema()
        {
            return datum_prijemaPac;
        }

        public string dajBrak()
        {
            return brakPac;
        }

        public string dajAdresu()
        {
            return adresaPac;
        }

        public char dajSpol()
        {
            return spolPac;
        }

        public string dajDatumRodjenja()
        {
            return datumrodjPac;
        }

        public void Hitan(bool hitan)
        {
            this.hitan = hitan;
        }

        public string dajIme()
        {
            return imePac;
        }

        public string dajPrezime()
        {
            return prezimePac;
        }

        public void Preimnuo(bool preminuo)
        {
            this.preminuo = preminuo;
        }

        public void dodajOrdinacije(List<int> zeljeneOrdinacije)
        {
            this.zeljeneOrdinacije = zeljeneOrdinacije;
        }


        public void prvaPomoc(string opis, string razlog)
        {
            this.Pp = new PrvaPomoc(opis, razlog);
        }

        public void prvaPomocPreminuo(string opis, string razlog, string vrijemeSmrti, string uzrokSmrti, string vrijemeObdukcije)
        {
            this.Pp = new PrvaPomoc(opis, razlog, vrijemeSmrti, uzrokSmrti, vrijemeObdukcije);
        }

        public string dajJMBG()
        {
            return jmbgPac;
        }

        public bool dajJelHitan()
        {
            return hitan;
        }

        public bool dajJelPreminuo()
        {
            return preminuo;
        }
    }
}
