﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Zadaca2.Model;

namespace Zadaca2
{
    public partial class LoginOsoblja : Form
    {
         Klinika klinika;

        public LoginOsoblja(Klinika k)
        {
            InitializeComponent();
            klinika = k;
        }

        private int ProvjeriOsoblje()
        {
            foreach (Osoblje os in klinika.Osoblje)
            {
                if (os.Sifra.Equals(textBox2.Text) && os.User.Equals(textBox1.Text))
                {
                    return 1;
                }
            }

            toolStripStatusLabel1.Text = ("Ne postojeci pristupni podaci. Pokusajte ponovo!");
            return 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (ProvjeriOsoblje() == 1)
            {
                this.Close();
                OsobljeForma osF = new OsobljeForma(klinika);
                osF.Show();
            }
            
        }
    }
}
