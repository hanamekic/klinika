﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Zadaca2.Model;

namespace Zadaca2
{
    public partial class OsobljeForma : Form
    {
         Klinika klinika;
        public OsobljeForma(Klinika k)
        {
            InitializeComponent();
            klinika = k;
        }

        private Pacijent pretraga()
        {
            return klinika.pretragaPoJMBG(textBox1.Text);
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            label3.Visible = true;
            label4.Visible = true;
            label6.Visible = true;
            textBox4.Visible = true;
            textBox5.Visible = true;
            radioButton3.Visible = true;
            radioButton4.Visible = true;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            Pacijent p = pretraga();
            p.staviPlacanjeNaNulu();
            p.dajZeljeneORdinacije().Clear();
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            Pacijent p = pretraga();
            p.Preimnuo(false);
            p.Hitan(true);
            p.prvaPomoc(textBox2.Text, textBox3.Text);
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            label7.Visible = true;
            label8.Visible = true;
            label9.Visible = true;
            textBox6.Visible = true;
            textBox7.Visible = true;
            textBox8.Visible = true;
            Pacijent p = pretraga();
            p.Preimnuo(true);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Pacijent p = pretraga();
            if (p.dajJelPreminuo())
            {
                p.prvaPomocPreminuo(textBox2.Text, textBox3.Text, textBox4.Text, textBox5.Text,textBox6.Text);
            }
            
            foreach(Control c in Controls)
            {
                if(c is TextBox)
                {
                    c.Text = "";
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            klinika.obrisiPacijenta(textBox1.Text);
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            Pacijent p = pretraga();
            List<Ordinacija> nova = new List<Ordinacija>();
            for(int i = 0; i < p.dajZeljeneORdinacije().Count; i++)
            {
                nova.Add(klinika.getById(p.dajZeljeneORdinacije()[i]));
            }
            nova.Sort(delegate (Ordinacija x, Ordinacija y)
            {
                return x.getBrojLjudi().CompareTo(y.getBrojLjudi());
            });

            for(int i = 0; i < nova.Count; i++)
            {
                richTextBox1.AppendText(i + 1 + ". " + nova[i].getIme());
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            richTextBox1.Visible = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            char c;
            if (radioButton1.Checked) c = 'z';
            else c = 'm';
            klinika.dodajPacijenta(textBox12.Text, textBox13.Text, textBox14.Text, textBox11.Text,c, textBox15.Text, textBox16.Text,Convert.ToString(DateTime.Now));
            Pacijent p = klinika.pretragaPoJMBG(textBox11.Text);
            p.dodajAnamnezu(textBox17.Text, textBox18.Text, textBox19.Text, textBox20.Text, textBox21.Text);
            klinika.dodajKarton(new Karton(p, null));
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Pacijent p = klinika.pretragaPoJMBG(textBox2.Text);
            textBox3.Visible = true;
            label5.Visible = true;
            textBox3.Text = (Convert.ToString(p.dajPlacanje()));
        }
    }
}
