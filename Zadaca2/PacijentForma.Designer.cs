﻿namespace Zadaca2
{
    partial class PacijentForma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode52 = new System.Windows.Forms.TreeNode("Ime: ");
            System.Windows.Forms.TreeNode treeNode53 = new System.Windows.Forms.TreeNode("Prezime:");
            System.Windows.Forms.TreeNode treeNode54 = new System.Windows.Forms.TreeNode("Maticni broj:");
            System.Windows.Forms.TreeNode treeNode55 = new System.Windows.Forms.TreeNode("Spol:");
            System.Windows.Forms.TreeNode treeNode56 = new System.Windows.Forms.TreeNode("Adresa:");
            System.Windows.Forms.TreeNode treeNode57 = new System.Windows.Forms.TreeNode("Datum rodjenja:");
            System.Windows.Forms.TreeNode treeNode58 = new System.Windows.Forms.TreeNode("Osnovni podaci", new System.Windows.Forms.TreeNode[] {
            treeNode52,
            treeNode53,
            treeNode54,
            treeNode55,
            treeNode56,
            treeNode57});
            System.Windows.Forms.TreeNode treeNode59 = new System.Windows.Forms.TreeNode("Trenutna bolest:");
            System.Windows.Forms.TreeNode treeNode60 = new System.Windows.Forms.TreeNode("Alegrije:");
            System.Windows.Forms.TreeNode treeNode61 = new System.Windows.Forms.TreeNode("Ranije bolesti:");
            System.Windows.Forms.TreeNode treeNode62 = new System.Windows.Forms.TreeNode("Stanje porodice:");
            System.Windows.Forms.TreeNode treeNode63 = new System.Windows.Forms.TreeNode("Zakljucak:");
            System.Windows.Forms.TreeNode treeNode64 = new System.Windows.Forms.TreeNode("Anamneza", new System.Windows.Forms.TreeNode[] {
            treeNode59,
            treeNode60,
            treeNode61,
            treeNode62,
            treeNode63});
            System.Windows.Forms.TreeNode treeNode65 = new System.Windows.Forms.TreeNode("Opis:");
            System.Windows.Forms.TreeNode treeNode66 = new System.Windows.Forms.TreeNode("Dugorocna:");
            System.Windows.Forms.TreeNode treeNode67 = new System.Windows.Forms.TreeNode("Datum propisivanja:");
            System.Windows.Forms.TreeNode treeNode68 = new System.Windows.Forms.TreeNode("Terapija", new System.Windows.Forms.TreeNode[] {
            treeNode65,
            treeNode66,
            treeNode67});
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Unesite svoj maticni broj:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(171, 33);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(146, 20);
            this.textBox1.TabIndex = 1;
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(44, 68);
            this.treeView1.Name = "treeView1";
            treeNode52.Name = "Node1";
            treeNode52.Text = "Ime: ";
            treeNode53.Name = "Node0";
            treeNode53.Text = "Prezime:";
            treeNode54.Name = "Node1";
            treeNode54.Text = "Maticni broj:";
            treeNode55.Name = "Node3";
            treeNode55.Text = "Spol:";
            treeNode56.Name = "Node4";
            treeNode56.Text = "Adresa:";
            treeNode57.Name = "Node5";
            treeNode57.Text = "Datum rodjenja:";
            treeNode58.Name = "Prezime:";
            treeNode58.Text = "Osnovni podaci";
            treeNode59.Name = "Node8";
            treeNode59.Text = "Trenutna bolest:";
            treeNode60.Name = "Node10";
            treeNode60.Text = "Alegrije:";
            treeNode61.Name = "Node11";
            treeNode61.Text = "Ranije bolesti:";
            treeNode62.Name = "Node12";
            treeNode62.Text = "Stanje porodice:";
            treeNode63.Name = "Node13";
            treeNode63.Text = "Zakljucak:";
            treeNode64.Name = "Node7";
            treeNode64.Text = "Anamneza";
            treeNode65.Name = "Node15";
            treeNode65.Text = "Opis:";
            treeNode66.Name = "Node16";
            treeNode66.Text = "Dugorocna:";
            treeNode67.Name = "Node17";
            treeNode67.Text = "Datum propisivanja:";
            treeNode68.Name = "Node14";
            treeNode68.Text = "Terapija";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode58,
            treeNode64,
            treeNode68});
            this.treeView1.Size = new System.Drawing.Size(233, 263);
            this.treeView1.TabIndex = 2;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numericUpDown1);
            this.groupBox1.Controls.Add(this.richTextBox1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(302, 68);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(228, 263);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Stanje u ordinacijama";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Unesite broj ordinacije:";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(9, 82);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(213, 158);
            this.richTextBox1.TabIndex = 2;
            this.richTextBox1.Text = "";
            this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.AllowDrop = true;
            this.numericUpDown1.Location = new System.Drawing.Point(126, 39);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(41, 20);
            this.numericUpDown1.TabIndex = 3;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(422, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(335, 31);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(63, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // PacijentForma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 343);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Name = "PacijentForma";
            this.Text = "PacijentForma";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
    }
}