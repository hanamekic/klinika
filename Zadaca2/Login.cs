﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Zadaca2.Model;

namespace Zadaca2
{
    public partial class Login : Form
    {
        Klinika klinika;

        public Login(Klinika k)
        {
            InitializeComponent();
            klinika = k;
        }

        private int ProvjeriDoktora()
        {

            foreach (Doktor dok in klinika.Doktori)
            {
                if (dok.User.Equals(textBox1.Text) && dok.Sifra.Equals(textBox2.Text))
                {
                    return 1;
                }
            }
            toolStripStatusLabel1.Text = ("Ne postojeci pristupni podaci. Pokusajte ponovo!");
            return 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(ProvjeriDoktora()==1)
            {
                this.Close();
                DoktorForma dokF = new DoktorForma(klinika);
                dokF.Show();
            }
        }
    }
}
