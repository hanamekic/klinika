﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zadaca2.Model;

namespace Zadaca2
{
    public class Klinika
    {
        List<Pacijent> pacijenti = new List<Pacijent>();
        List<Karton> kartoni = new List<Karton>();
        List<Ordinacija> ordinacije = new List<Ordinacija>{
            new Ordinacija("Ortopedija", 0, false, false, 1),
            new Ordinacija("Kardiologija", 0, false, false, 2),
            new Ordinacija("Dermatologija", 0, false, false, 3),
            new Ordinacija("Interna", 0, false, false, 4),
            new Ordinacija("Otorinolaringologija", 0, false, false, 5),
            new Ordinacija("Oftamologija", 0, false, false, 6),
            new Ordinacija("Laboratorija", 0, false, false, 7),
            new Ordinacija("Hirurgija", 0, false, false, 8),
            new Ordinacija("Stomatologija", 0, false, false, 9),
            new Ordinacija("Opsta medicina", 0, false, false, 10)
        };
        List<Doktor> doktori = new List<Doktor>
        {
            new Doktor("Haso","Hasic",1000,1,"doktor11","kk"),
            new Doktor("Mujo","Mujic",2000,2,"doktor22","ko"),
            new Doktor("Hana","Hanic",1500,3,"doktor33","ok"),
            new Doktor("Marko","Markic",1000,4,"doktor44","gg"),
            new Doktor("Emir","Emirkovic",1200,5,"doktor55","rr"),
            new Doktor("Aida","Idica",1500,6,"doktor66","tt"),
            new Doktor("Ema","Emic",1400,7,"doktor77","yy"),
            new Doktor("Samir","Samkic",2000,8,"doktor88","yr"),
            new Doktor("Emira","Suljic",1800,9,"doktor99","gh"),
            new Doktor("Lejla","Lelic",1300,10,"doktor10","rf")
        };
        List<Osoblje> osoblje = new List<Osoblje>
        {
            new Osoblje("Sestra","Haic","0204956183323","sestra11","kk"),
            new Osoblje("Brat","Mulic","1203978183725","brat22","ko"),
            new Osoblje("Hanka","Hankic","14022987726452","seka33","ok"),
            new Osoblje("Mrko","Mrkic","0203988726152","braco44","gg")
        };
        
        public List<Osoblje> Osoblje { get => osoblje; set => osoblje = value; }
        public List<Doktor> Doktori { get => doktori; set => doktori = value; }
        public List<Pacijent> Pacijenti { get => pacijenti; set => pacijenti = value; }
        public List<Ordinacija> Ordinacije { get => ordinacije; set => ordinacije = value; }


        public Pacijent dodajPacijenta(string ime, string prezime, string datumrodj, string jmbg, char spol, string adresa, string brak, string datum_prijema)
        {
            Pacijent p = new Pacijent(ime, prezime, datumrodj, jmbg, spol, adresa, brak, datum_prijema);
            pacijenti.Add(p);
            return p;
        }

        public bool obrisiPacijenta(string jmbg)
        {
            for (int i = 0; i < pacijenti.Count; i++)
            {
                if (kartoni[i].dajPacijentaIzKartona().dajJMBG().Equals(jmbg))
                {
                    kartoni.Remove(kartoni[i]);
                    return true;
                }
            }
            return false;
        }


        public Pacijent pretragaPoJMBG(string jmbg)
        {
            for (int i = 0; i < pacijenti.Count; i++)
            {
                if (pacijenti[i].dajJMBG().Equals(jmbg))
                {
                    return pacijenti[i];
                }
            }
            return null;
        }

        public Karton pretragaPoKartonima(string jmbg)
        {
            for (int i = 0; i < kartoni.Count; i++)
            {
                if (kartoni[i].dajPacijentaIzKartona().dajJMBG().Equals(jmbg))
                {
                    return kartoni[i];
                }
            }
            return null;
        }

        public Doktor pretragaDoktora(int id)
        {
            for (int i = 0; i < doktori.Count; i++)
            {
                if (doktori[i].Soba.Equals(id))
                {
                    return doktori[i];
                }
            }
            return null;
        }


        public void dodajKarton(Karton k)
        {
            kartoni.Add(k);
        }

        public Ordinacija getById(int id)
        {
            return ordinacije[id - 1];
        }

        public bool aparat(int id)
        {
            return ordinacije[id - 1].radiLiAparat();
        }
        /*
        public int plata(int id)
        {
            return ordinacije[id - 1].povecajPlatu();
        }
        */
        public bool doktor(int id)
        {
            return ordinacije[id - 1].imaLiDoktora();
        }


        public void smanjiRedCekanja(int id)
        {
            ordinacije[id - 1].dekrement();
        }

        public void incrementById(int id)
        {
            ordinacije[id - 1].inkrement();
        }
    }
}
