﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Zadaca2.Model;

namespace Zadaca2
{
    public partial class InfoOSmrti : Form
    {
         Klinika klinika;


        public InfoOSmrti(Klinika k)
        {
            InitializeComponent();
            klinika = k;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void prikaz(Pacijent p)
        {
            textBox1.Text = (p.Pp.dajVrijemeSmrti());
            textBox2.Text = (p.Pp.dajUzrok());
            textBox3.Text = (p.Pp.dajVrijemeObdukcije());
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
