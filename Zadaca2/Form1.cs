﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zadaca2
{
  
    public partial class Form1 : Form
    {
         Klinika klinika;
        public Form1(Klinika k)
        {
            InitializeComponent();
            klinika=k;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Login log = new Login(klinika);
            log.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            LoginOsoblja log = new LoginOsoblja(klinika);
            log.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            PacijentForma f = new PacijentForma(klinika);
            f.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.DoubleBuffered = true;
            this.Paint += new PaintEventHandler(Form1_Paint);
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            string s = "NMK";
            Rectangle rec = new Rectangle(475, 75, 50, 25);
            e.Graphics.DrawRectangle(Pens.Black, new Rectangle(450, 50, 100, 100));
            e.Graphics.DrawString(s, this.Font, Brushes.Red, 480, 80);
            e.Graphics.DrawRectangle(Pens.Green,rec );
            e.Graphics.DrawRectangle(Pens.Green, new Rectangle(475, 75, 25, 50));
            e.Graphics.DrawArc(Pens.Red, rec, 0.0f, 90.0f);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Refresh();
        }
    }
}
